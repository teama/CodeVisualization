#pragma once

#include <vector>
#include <string>
using namespace std;

class PieRenderer
{
    public:

        PieRenderer(){}
        void			draw();     // draws the 3D pie
        void            display_text();
        void            color();    // set random colors in a list to assign to each 3D pie block
        void            updateDisplay(float rgb_list[]);  // to update display on clicking a pie
        void            DisplayNumClones(float rgb_list[]);     // to Display # of Clones for selected color
        void            updateWindowTitle();    // to update window title to selected file's path
        void            displayPrevDir();       // to display previous directory
    protected:
        vector<float> color_list;   // list to store random colors
        vector<string>  lastDir;    // to store directories clicked
};





