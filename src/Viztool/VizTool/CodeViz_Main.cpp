#include <windows.h>
#include <math.h>										//For abs(), pow()
#include <GL/glut.h>									//GLUT library
#include "PieRenderer.h"
#include "zpr.h"
#include <iostream>
#include <memory.h>
#include <string>
#include "ReadData.h"

using namespace std;

int WIDTH=600;
int HEIGHT=600;

float fov	 = 60;										//Perspective projection parameters
float aspect = 1;
float z_near = 0.1;
float z_far  = 3000;

float eye_x = 0, eye_y = WIDTH/10, eye_z = HEIGHT/10;				//Modelview (camera extrinsic) parameters
float c_x   = 0,  c_y   = 0,  c_z   = 0;
float up_x  = 0,  up_y  = 1,  up_z  = 0;

string fileNum="1";     // provide first file # to be read

PieRenderer		renderer;
void displayText()
{
    cout<<endl;
    cout<<" Use the following for navigation: "<<endl;
    cout<<" Rotate  -  Left mouse click "<<endl;
    cout<<" Zoom    -  Middle mouse click "<<endl;
    cout<<" Pan     -  Right mouse click "<<endl;
    cout<<"--------------------------------------------"<<endl;
    cout<<" Use the following for interaction with Pie:"<<endl;
    cout<<" Select Pie for subdirectory   -  Left mouse click "<<endl;
    cout<<" Display number of Clones      -  Right mouse click "<<endl;
    cout<<" Go back to previous directory -  Hit 'd' or 'D' "<<endl;
    cout<<endl;
}

//function to call when resizing window
void viewing(int W, int H)								//Window resize function, sets up viewing parameters (GLUT callback function)
{
	glViewport(0,0,W,H);
	glMatrixMode (GL_PROJECTION);						// Set the projection matrix
	glLoadIdentity ();

	gluPerspective(fov,float(W)/H,z_near,z_far);
    glMatrixMode(GL_MODELVIEW);							// Set the modelview matrix (including the camera position and view direction)
	glLoadIdentity ();
	gluLookAt(eye_x,eye_y,eye_z,c_x,c_y,c_z,up_x,up_y,up_z);
}

//function to call draw function in PieRenderer to draw the 3D Pie
void draw()
{
	renderer.draw();
}

//to read keyboard input
void displayPrevDir(unsigned char key, int x, int y)
{
    // if 'd' or 'D' is hit, call function to update display to previous directory
    if ((key=='d')||(key=='D'))
    {
        renderer.displayPrevDir();
    }
}

// function to check color of pixel when left mouse button is clicked and update the display accordingly
void color_pick(int button, int state, int x, int y)
{
    float pick[3];  // to store RGB values
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    // if mouse is clicked then check RGB value at that pixel and call appropriate function
    glReadPixels(x , viewport[3]-y , 1 , 1 , GL_RGBA , GL_FLOAT , &pick);    // reads pixels from x,y location and stores in pick

    // call zprMouse if clicked on white area
    if((pick[0]==1)&&(pick[1]==1)&&(pick[2]==1))
    {zprMouse(button,state,x,y);}

    //call update function if any other color is clicked
    else
    {
        pick[0]=floor(pick[0]*10+0.1)/10;   // stores R value
        pick[1]=floor(pick[1]*10+0.1)/10;   //stores G value
        pick[2]=floor(pick[2]*10+0.1)/10;   // stores B value
        // if mouse button clicked then call update
        if (state==GLUT_UP)
        {
            if (button==GLUT_LEFT_BUTTON)
            {
                renderer.updateDisplay(pick);      // call function to update the display
            }
            else if (button==GLUT_RIGHT_BUTTON)
            {
                renderer.DisplayNumClones(pick);      // call function display # of clones
            }
        }
    }
    glutPostRedisplay();    // update Display
}

int main(int argc,char* argv[])							//Main program
{
    glutInit(&argc, argv);								// Initialize the GLUT toolkit
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);		// Ask GLUT to create next windows with a RGB framebuffer and a Z-buffer too
    glutInitWindowSize(WIDTH,HEIGHT);					// Tell GLUT how large are the windows we want to create next
    glutCreateWindow("");          				        // Create our window
    displayText();                                      // to display information
    glutDisplayFunc(draw);								// Add a drawing callback to the window
    zprInit(c_x,c_y,c_z);
    glutMouseFunc(color_pick);                           // Calls the function color_pick when mouse buttons are clicked
    //glutMouseFunc(zprMouse);                           // handles mouse clicks but doesn't update display when clicked on color
    glutMotionFunc(zprMotion);                           // handles zoom,pan,rotate motion
    glutReshapeFunc(viewing);							// Add a resize callback to the window
    glutKeyboardFunc(displayPrevDir);                   // To read keyboard input
    glutMainLoop();										// Start the event loop that displays the graph and handles window-resize events

    return 0;
}



