#pragma once

#include <vector>
#include <iostream>

using namespace std;

class ReadData
{
    struct DataVal      // structure to store data values
    {
         double percent;
         double numClones;
         string nextFile;

    }datavalues;


    public:
        ReadData(string file)
        {
           ReadDataVal(file);
        }
        string pathname;
        int ReadDataVal(string file);  // to read data from files
        void printData();   // print data
        void clearData();   // clear the data
        double getPercent(int i);  // gets percentage at given point
        double getHeight(int i);  // gets # of clones at given point
        string getNextFile(int i);  // gets the next directory file at given point
        int getSize()                   // returns size of data read
        {return data_val.size();}
        string getPathname();       // function to get pathname of file

   protected:
       vector<DataVal> data_val;    // vector list to store data values read from file

};


