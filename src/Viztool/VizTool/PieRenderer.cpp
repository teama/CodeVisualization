/*
This program draws a 3D Pie to visualize code clones in a directory.

Each pie angle represents the total number of code lines in a directory files
and the height represents the number of code clones in that directory

Each 3D pie block consists of top triangle, bottom triangle,two side quads, and several quads to cover the curved part

*/

#include "PieRenderer.h"
#include "ReadData.h"
#include <GL/glut.h>
#include <algorithm>
#include <math.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <string>
#include <vector>
using namespace std;

string numFile="1";         // starting file number to read data
ReadData data(numFile);     // create ReadData object

// Function to calculate random RGB values and store them in a list
void PieRenderer::color()
{
    color_list.clear();
    float frequency = 1;
    float red,blue,green;
    for (int i = 0; i < data.getSize(); ++i)
    {
        red   = floor(((sin(frequency*i + 0) * 127 + 128)/256)*10+0.1)/10;
        green = floor(((sin(frequency*i + 2) * 127 + 128)/256)*10+0.1)/10;
        blue  = floor(((sin(frequency*i + 4) * 127 + 128)/256)*10+0.1)/10;
        color_list.push_back(red);
        color_list.push_back(green);
        color_list.push_back(blue);
    }
}

// To update the window title to selected file's path
void PieRenderer::updateWindowTitle()
{
    color();
    if(lastDir.size()==0){lastDir.push_back(numFile);}
    string str="Code Clone Viz:  "+data.getPathname();
    char * c=const_cast<char*>(str.c_str());
    glutSetWindowTitle(c);      // update Window title to path of selected file

}

void PieRenderer::displayPrevDir()
{
    if(lastDir.size()>0)
    {
        data.clearData();       // clear previous data
        string file;
        file=lastDir[lastDir.size()-1];     // get last saved file #
        lastDir.pop_back();             // delete last element
        data.ReadDataVal(file);    // update data with new file values
        updateWindowTitle();        // update window title
    }
        glutPostRedisplay();
}

// To display # of clones of selected pie
void PieRenderer::DisplayNumClones(float rgb[])
{
    double numClone;

    for(int i=0;i<color_list.size();i=i+3)
    {
        if ((color_list[i]==rgb[0])&&(color_list[i+1]==rgb[1])&&(color_list[i+2]==rgb[2]))
        {
            double numClone=data.getHeight(i/3);
            cout<<"Number of Clones: "<<numClone<<endl;
            break;
        }
    }
}

// Function checks the color of pie clicked and update display to the subdirectory selected
void PieRenderer::updateDisplay(float rgb[])
{
    string file;   // variable to get file number

    // loop to find index of selected color
    for(int i=0;i<color_list.size();i=i+3)
    {
        if ((color_list[i]==rgb[0])&&(color_list[i+1]==rgb[1])&&(color_list[i+2]==rgb[2]))
        {
            file = (data.getNextFile(i/3));     //get the file number corresponding to the color
            if(lastDir[lastDir.size()-1]!=file)
            {lastDir.push_back(file);}  // store this directory or file #

            // read data and update only if file exists i.e. file# > 0
            if(file!="0")
            {
                data.clearData();       // clear previous data
                data.ReadDataVal(file);    // update data with new file values
                updateWindowTitle();        // update window title
                break;
            }
            else{break;}    // break to get out of loop
        }
    }
}

// To draw the 3D Pie
void PieRenderer::draw()
{
	glClearColor(1.0f,1.0f,1.0f,1.0f);								// Clear the frame and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearDepth(3.0f);

    //color();    // call the function to set colors for pie's
    updateWindowTitle();    // update window title
    const double radius = 30; // The radius of the disk
    double start_angle = 0,end_angle = 0, angle = 0;  // variable to store start angle and end angle for triangle fan
    double x,y,z;       //x,y and z coordinates for vertex
    int numSegments;    // variable to store # of segments in a fan
    double seg_factor;  // variable to get next point in fan

    int list_size=data.getSize();    //get size of percent_list list

    double angle_list[list_size+1];   //declare new list to store angles
    int angle_list_size=list_size+1;  // to calculate size of angle_list
    angle_list[0]=0;                  // set first angle to 0

    int color=0;                      // variable to navigate through color_list

    vector<double> xcoord_list;       // list to store x coordinates of all vertices of triangle fan
    vector<double> ycoord_list;       // list to store y coordinates of all vertices of triangle fan
    vector<double> xcoord_sidelist;     // list to store x coordinates of end points of triangle fans to create side quads
    vector<double> ycoord_sidelist;     // list to store y coordinates of end points of triangle fans to create side quads


    //Triangle_fan method to draw the Pie Chart
    glColor3f(0,1,0);
    glShadeModel(GL_FLAT);      // to set shade model
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);       //sets the triangle fan mode to fill

    // loop to convert % to angles and stores them in list
    for(int i=1;i<list_size+1;i++)
    {
        angle_list[i]= ((data.getPercent(i-1)*360)/100);  //convert % to angle
    }

    //to draw bottom triangle fan
    for (int i = 0; i < angle_list_size; i++)       //loop to go through the angles in angle_list
    {
        x=0;y=0;z=0;        // set x, y and z to origin

        glBegin(GL_TRIANGLE_FAN);

        //sets color for triangle fan
        glColor4f(color_list[color],color_list[color+1], color_list[color+2],1.0f);

        glVertex3f(x,y,z);      //set the starting vertex for triangle fan at origin

        start_angle+=(angle_list[i]*M_PI/180);                  //set the starting angle in radians
        end_angle=start_angle+(angle_list[i+1]*M_PI/180);     //set the ending angle in radians

        angle=start_angle;                      // starting angle for triangle fan that will increment in while loop
        numSegments = angle_list[i+1];      //sets number of segments in a fan based on the angle

        seg_factor=((angle_list[i+1]*M_PI/180)/numSegments);     //segment factor in triangle fan to calculate next vertex in fan

        while(angle<=end_angle)   //loop to create triangle fan till end_angle is reached
        {
            x= radius*cos(angle);           //calculates x coordinate of vertex
            y= radius*sin(angle);          //calculates y coordinate of vertex
            angle+= seg_factor;           // add seg_factor to angle
            glVertex3f(x,y,z);             // coordinates for next fan vertex

            // add the x and y coordinate to a vector to draw the last curve quad part
            xcoord_list.push_back(x);
            ycoord_list.push_back(y);
        }

        glEnd();


        // draw quads for the curve part in each segment in the triangle fan
        glColor4f(color_list[color]-0.05,color_list[color+1]-0.05, color_list[color+2]-0.05,1.0f);
        for(int j=0;j< xcoord_list.size()-1;j=j+1)
        {
            glPushMatrix();
            glBegin(GL_QUADS);
            //four vertices for quad
            glVertex3f( xcoord_list[j], ycoord_list[j],0);
            glVertex3f( xcoord_list[j+1], ycoord_list[j+1],0);
            glVertex3f( xcoord_list[j+1], ycoord_list[j+1],-data.getHeight(i));
            glVertex3f( xcoord_list[j], ycoord_list[j],-data.getHeight(i));
            glEnd();
            glPopMatrix();

        }
            //clears the coordinate list so that it can be repopulated for each triangle fan
            xcoord_list.clear();
            ycoord_list.clear();

            // calculates and stores the end points of each triangle fan in a list to create side quads
            double c_x=radius*cos(start_angle);
            double c_y=radius*sin(start_angle);
            xcoord_sidelist.push_back(c_x);         // push x coordinate
            ycoord_sidelist.push_back(c_y);       // push y coordinate

            color+= 3;      // increment color index by 3 to get next set of RGB values
      }


        // draws the top triangle fan
        color=0;        // reset color index
        start_angle=0;
        end_angle =0;
        for (int i = 0; i < angle_list_size; i++)       //loop to go through the angles in angle_list
        {
            x=0;
            y=0;
            z=-data.getHeight(i);
            glBegin(GL_TRIANGLE_FAN);

            //sets color for triangle fan
            glColor4f(color_list[color],color_list[color+1], color_list[color+2],1.0f);
            color+= 3;      // increment color index by 3 to get next set of RGB values

            glVertex3f(x,y,z);      //set the starting vertex for triangle fan at origin

            start_angle+=(angle_list[i]*M_PI/180);                  //set the starting angle in radians
            end_angle=start_angle+(angle_list[i+1]*M_PI/180);     //set the ending angle in radians

            angle=start_angle;                      // starting angle for triangle fan that will increment in while loop
            numSegments = angle_list[i+1];      //sets number of segments in a fan based on the angle

            seg_factor=((angle_list[i+1]*M_PI/180)/numSegments);     //segment factor in triangle fan to calculate next vertex in fan

            while(angle<=end_angle)   //loop to create triangle fan till end_angle is reached
            {
                x= radius*cos(angle);           // calculates x coordinate of vertex
                y= radius*sin(angle);          //calculates y coordinate of vertex
                angle+= seg_factor;           // add seg_factor to angle
                glVertex3f(x,y,z);             // coordinates for next fan vertex
            }

            glEnd();
        }

        // two for loops to draw side quads of each pie
        // NOTE: display driver crashes and gives error if put in separate function to draw these side quads
        // Same error if both quads generated in same for loop


        color=0;        // reset color index
        for (int i = 0; i < data.getSize(); i=i+1)       //loop to go through the angles in angle_list
        {
            x=0;y=0;z=0;
            // sets the color of quad
            glColor4f(color_list[color]-0.05,color_list[color+1]-0.05, color_list[color+2]-0.05,1.0f);
            color+= 3;      // increment color index by 3 to get next set of RGB values
            // draw only if height > 0
            if(data.getHeight(i)>0)
            {
                // draws the  side quads of each triangle fan
                glBegin(GL_QUADS);

                glVertex3f(x,y,z);
                glVertex3f(xcoord_sidelist[i],ycoord_sidelist[i],z);
                glVertex3f(xcoord_sidelist[i],ycoord_sidelist[i],-data.getHeight(i));
                glVertex3f(x,y,-data.getHeight(i));

                glEnd();
            }
        }

        color=0;            // reset color index
        for (int i = 0; i < data.getSize(); i=i+1)       //loop to go through the angles in angle_list
        {
            x=0;y=0;z=0;
            // sets the color of quad
            glColor4f(color_list[color]-0.05,color_list[color+1]-0.05, color_list[color+2]-0.05,1.0f);
            color+= 3;      // increment color index by 3 to get next set of RGB values
            // draw only if height > 0
            if(data.getHeight(i)>0)
            {
                // draws the other side quads of each triangle fan
                glBegin(GL_QUADS);

                glVertex3f(x,y,0);
                glVertex3f(xcoord_sidelist[i+1],ycoord_sidelist[i+1],z);
                glVertex3f(xcoord_sidelist[i+1],ycoord_sidelist[i+1],-data.getHeight(i));
                glVertex3f(x,y,-data.getHeight(i));

                glEnd();
            }
        }

	glutSwapBuffers();
}




