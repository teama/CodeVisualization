#include "ReadData.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <typeinfo>
#include <math.h>
#include <string.h>


using namespace std;

// Reads the data from file and split every line into tokens. Stores the values in a list
int ReadData::ReadDataVal(string fNum)
{
    DataVal datavalues;
    string file="data/"+fNum+".txt";
    string line;    // variable to store line read from file
    ifstream myfile ;
    myfile.open(file.c_str());
    string col1,col2,col3,nextFile;       // variable to store values
    double percent,numClones;  // variables to store percentage and total # of lines
    if (myfile)
    {
        getline(myfile,line,'\n');     // get top line as pathname
        pathname=line;

        // loop that reads every line in a file and split it into tokens and stores them in a list
        while ( getline (myfile,line) )
        {
            istringstream ss(line);
            getline(ss,col1,' ');      // split first token
            getline(ss,col2,' ');      // split second token
            getline(ss,col3,' ');      // split second token

            istringstream s1(col1);
            s1>>percent;
            datavalues.percent=percent;     // store percentage values in struct

            istringstream s2(col2);
            s2>>numClones;
            datavalues.numClones=numClones;     // store numClones in struct

            istringstream s3(col3);
            s3>>nextFile;
            datavalues.nextFile=nextFile;

            data_val.push_back(datavalues);     // push percentage and numClones in data_val list
        }
    }

    else
    {
        cout<<"No File Read"<<endl;
        return 1;       // exit if no file read
    }

    myfile.close();     // close the file
    return 0;

}

// To print data read from file
void ReadData::printData()
{
    for(int i=0;i<data_val.size();i++)
    {
        cout<<data_val[i].percent<<"    "<<data_val[i].numClones<<"     "<<data_val[i].nextFile<<endl;
    }
}

// To clear data from file
void ReadData::clearData()
{
    data_val.clear();
}

// To get percent data from list
double ReadData::getPercent(int i)
{
    return (data_val[i].percent);
}

// To get # of clones (--> height) from list
double ReadData::getHeight(int i)
{
    return ((data_val[i].numClones));
}

// To get subdirectory file # from list
string ReadData::getNextFile(int i)
{
    return (data_val[i].nextFile);
}

// To get pathname
string ReadData::getPathname()
{
    return pathname;
}

