/**
 * @author Naveen Kumar Gorla
 * @version 1.0 (Initial Version)
 * @category Junit Test Cases
 * @since 5-Oct-2015
 */

package org.agile6990.junits;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;


public class ReadXMLTest {

	/**
	 * Create a valid test case if the given file exists in the target path.
	 * @result This test gets passed if the "clone.xml" file exists in the shared path,
	 *         and file.exists() decide the validity of the file existence
	 */
	@Test
	public void CloneFileTest() {
		File file = new File("clone.xml");
		assertTrue(file.exists());
	}
	
	@Test
	public void CreateFileTest() {
		File file = new File("StrtEnd.txt");
		assertTrue(file.exists());
	}
	
	/*@Test
	public void WriteFileTest() {
		File file = new File("StrtEnd.txt");
		assertTrue(file.canWrite());
	}*/
	
}