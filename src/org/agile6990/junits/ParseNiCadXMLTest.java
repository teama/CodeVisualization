/**
 * @author Naveen Kumar Gorla
 * @version 1.0 (Initial Version)
 * @category Junit Test Cases
 * @since 5-Oct-2015
 */

package org.agile6990.junits;

import java.util.List;

import static org.junit.Assert.*;

//import org.agile6990.ParseXML.CloneCounter;
import org.agile6990.ParseXML.LineCounterOfFolders;
import org.agile6990.model.ViewInput;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ParseNiCadXMLTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testCloneCount() 
	{
		//setup 
		int expectedCloneCount=2019;
		//execute
		int resultedCloneCount=new LineCounterOfFolders().xmlPraseForNicadResult("NiCadOutPut/cobertura-1.9.4.1_blocks-clones/cobertura-1.9.4.1_blocks-clones-0.0.xml").size();
		//assert
		assertEquals(expectedCloneCount,resultedCloneCount);
	}
	
	@Test
	public void testFilewiseLineCount(){
		int expectedCloneCount=650;
		int LineCount = 0;
		List<ViewInput> ListFileWiseLineCount = new LineCounterOfFolders().GetFileWiseLineCount("./NiCadOutPut/cobertura-1.9.4.1_blocks_wellFormed.xml", false);
		for(int i = 0; i < ListFileWiseLineCount.size(); i++)
		{
			if(ListFileWiseLineCount.get(i).fullDirectory.contains("../QualitasCorpus-20130901r/Systems/cobertura/cobertura-1.9.4.1/src/cobertura-1.9.4.1/src/net/sourceforge/cobertura/javancss/parser/java15/JavaCharStream.java"))
			{
				LineCount=ListFileWiseLineCount.get(i).lineCount;
				//System.out.println(LineCount);
				break;
			}			
		}
		assertEquals(expectedCloneCount,LineCount);
	}

}