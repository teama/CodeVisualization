package org.agile6990.model;

import java.util.Comparator;

//import org.zibran.lime.model.Sizeable;

public class ComparatorByIncreasingSize implements Comparator<Sizeable> {
	private static ComparatorByIncreasingSize comparator = null;

	private ComparatorByIncreasingSize() {
	}

	public static ComparatorByIncreasingSize getInstance() {
		if (comparator == null) {
			comparator = new ComparatorByIncreasingSize();
		}
		return comparator;
	}

	@Override
	public int compare(Sizeable ob1, Sizeable ob2) {
		return ob1.getSize() - ob2.getSize();
	}
}