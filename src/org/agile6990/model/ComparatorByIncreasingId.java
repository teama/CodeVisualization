package org.agile6990.model;

import java.util.Comparator;

import org.agile6990.model.Identifiable;

public class ComparatorByIncreasingId implements Comparator<Identifiable> {
	private static ComparatorByIncreasingId comparator = null;

	private ComparatorByIncreasingId() {
	}

	public static ComparatorByIncreasingId getInstance() {
		if (comparator == null) {
			comparator = new ComparatorByIncreasingId();
		}
		return comparator;
	}

	@Override
	public int compare(Identifiable ob1, Identifiable ob2) {
		return ob1.getId() - ob2.getId();
	}
}
