package org.agile6990.model;

public interface Sizeable {
	public int getSize();
}
