package org.agile6990.model;

//import org.apache.logging.log4j.LogManager;
//import org.apache.;
//import org.apache.logging.log4j.Logger;
//import org.zibran.lime.TestCodeFragment;
//import org.zibran.lime.core.Key;

public class CodeFragment implements Sizeable, Identifiable {
//public class CodeFragment  {	// private static final Logger LOGGER =
	// LogManager.getLogger(CodeFragment.class);

	String srcFile = "";
	String content = "";
	int startLine = -1;
	int endLine = -1;
	int size = -1;
	int id = -1;

	// Key[] fingerPrintedContent;
	// public Key[] getFingerPrintedContent() {
	// return fingerPrintedContent;
	// }
	//
	// public void setFingerPrintedContent(Key[] fingerPrintedContent) {
	// this.fingerPrintedContent = fingerPrintedContent;
	// }

	public CodeFragment(int id, String srcFile, int startLine, int endLine, String content) {
		this.id = id;
		this.srcFile = srcFile;
		this.startLine = startLine;
		this.endLine = endLine;
		this.content = content.trim();
		// LOGGER.debug("setting size = content.length");
		this.size = content.split("\n").length;
	}

	public CodeFragment(int id, String srcFile, int startLine, int endLine) {
		this.id = id;
		this.srcFile = srcFile;
		this.startLine = startLine;
		this.endLine = endLine;
		// LOGGER.trace("setting size = endLine - startLine + 1");
		this.size = endLine - startLine + 1;
	}

	public CodeFragment(String srcFile, int startLine, int endLine) {
		this.srcFile = srcFile;
		this.startLine = startLine;
		this.endLine = endLine;

		// LOGGER.trace("setting size = endLine - startLine + 1");
		this.size = endLine - startLine + 1;
	}

	public String getSrcFile() {
		return srcFile;
	}

	public void setSrcFile(String srcFile) {
		this.srcFile = srcFile;
	}

	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		// LOGGER.trace("setting size = content.length");

		this.size = content.split("\n").length;
		// LOGGER.trace("id=" + id + ", Size=" + size + ", Content = " +
		// content);

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + endLine;
		result = prime * result + id;
		result = prime * result + size;
		result = prime * result + ((srcFile == null) ? 0 : srcFile.hashCode());
		result = prime * result + startLine;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodeFragment other = (CodeFragment) obj;
		if (endLine != other.endLine)
			return false;
		if (id != other.id)
			return false;
		// if (size != other.size)
		// return false;
		if (srcFile == null) {
			if (other.srcFile != null)
				return false;
		} else if (!srcFile.equals(other.srcFile))
			return false;
		if (startLine != other.startLine)
			return false;
		return true;
	}

	public boolean isCompletelyPopulated() {
		if (id < 0)
			return false;
		if (startLine < 0)
			return false;
		if (endLine < 0)
			return false;
		if (size < 0)
			return false;
		if (srcFile.length() == 0)
			return false;
		if (content.length() == 0)
			return false;

		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" id = " + id);
		sb.append("\n srcFile = " + srcFile);
		sb.append("\n start = " + startLine);
		sb.append("\n end = " + endLine);
		sb.append("\n size = " + size);
		sb.append("\n content = \n" + content);

		return sb.toString();
	}
}