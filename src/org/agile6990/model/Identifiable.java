package org.agile6990.model;

public interface Identifiable {
	public int getId();
}
