package org.agile6990.ParseXML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.agile6990.ParseXML.LineCounterOfFolders;
import org.agile6990.model.CodeFragment;
import org.agile6990.model.ViewInput;

/**
 *This program is able to output/generate 
 * 1. File wise clone and line count
 * 2. Folder wise clone count 
 * For first output, input is as below
 * @input parameter 1 'NiCad Block XML file' 'CloneType1 XML File' 'CloneType3 XML File' 'CloneType2 XML File'
 * For second output only input 0; 
 * @author Rakib
 * 
 */

public class ParseNiCadXML {
	public static void main(String[] args) {
		
		String NiCadFileForFileWiseLineCount="";
		String NiCadFileForType1Clone="";
		String NiCadFileForType2Clone="";
		String NiCadFileForType3Clone="";
		String viewInputFilePath="";
		String folderLsitFilePath="";

		/** reading arguments for input files**/
		
		//This first argument decide which task to perform; If its 1 then count file wise line and clone count; 
		//Otherwise Folder wise clone count
		String isGenerateViewInput = args[0];
		
		//Getting the input for all required file
		if(isGenerateViewInput=="1")
		{
			NiCadFileForFileWiseLineCount = args[1];
			NiCadFileForType1Clone = args[2];
			NiCadFileForType2Clone = args[3];
			NiCadFileForType3Clone = args[4];
		}
		else
		{	
			viewInputFilePath=args[1];
			folderLsitFilePath=args[2];
		}
		
		boolean isDom = false;
		
		ParseNiCadXML parseNicadXML=new ParseNiCadXML();
		
		if(isGenerateViewInput=="1")
		{
			parseNicadXML.GenerateViewInput(NiCadFileForType1Clone,NiCadFileForType2Clone,NiCadFileForType3Clone,NiCadFileForFileWiseLineCount,isDom);
		}
		else
		{
			parseNicadXML.GenerateFolderWiseCloneCount(viewInputFilePath,folderLsitFilePath);
		}
	}
	
	//This method generates a text file contains folder wise clone count, based on input file of 'ViewInput.txt' and 'tokens.txt'
	public void GenerateFolderWiseCloneCount(String viewInputFilePath, String folderLsitFilePath)
	{
		String  folderWiseCloneCount="";
		
		List<ViewInput> viewInputList=GetViewInput(viewInputFilePath);
        ArrayList<String> FolderList=GetFolderList(folderLsitFilePath);
			
		
		List<ViewInput> FolderWiseCloneCount=new ArrayList<ViewInput>();
		
		for(int i=0;i<FolderList.size();i++)
		{	
			ViewInput curentfolder=new ViewInput();
			
			curentfolder.fullDirectory=FolderList.get(i).toString();
			
			for(ViewInput viewInput:viewInputList )
			{
				if(viewInput.fullDirectory.contains(FolderList.get(i).toString()))
				{
					
					curentfolder.cloneCount+=viewInput.cloneCount;
				}
			}
			
			FolderWiseCloneCount.add(curentfolder);
		}
		
		
		for(ViewInput viewInput:FolderWiseCloneCount )
		{ 
		folderWiseCloneCount=folderWiseCloneCount+viewInput.cloneCount+" "+ viewInput.fullDirectory+"\n"; 
		}
		/** Writing viewinput object to file**/
		try 
		{
			String fileName="FolderWiseCloneCount";
			
			FileWriter fileWriter = new FileWriter(fileName, false);													
			fileWriter.write(folderWiseCloneCount);
			fileWriter.close();
		} catch (IOException ioe) 
		{
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
	
	//Reading file to get folder list from 'token.txt'
	public ArrayList<String> GetFolderList(String folderFilePath)
	{
	    ArrayList<String> folderList = new ArrayList<String>();
	    
		try {
			File file = new File(folderFilePath);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			String line;
			int i=0;
			
			while ((line = bufferedReader.readLine()) != null) 
			{
				folderList.add(line.trim());
				i=i+1;
			}
			
			fileReader.close();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}

		return folderList;
	}
	
	//This method read the file wise  line and clone count file
	public List<ViewInput> GetViewInput(String viewFilePath)
	{
		List<ViewInput> viewInputList = new ArrayList<ViewInput>();
		
		try {
			File file = new File(viewFilePath);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			String line;
			
			while ((line = bufferedReader.readLine()) != null) {
				String [] elementOfViewInput=line.split("\\s+");
				
				ViewInput viewInput=new ViewInput();
				viewInput.fileName=elementOfViewInput[0];
				viewInput.fullDirectory=elementOfViewInput[1];
				viewInput.lineCount=Integer.valueOf(elementOfViewInput[2]);
				viewInput.cloneCount=Integer.valueOf(elementOfViewInput[3]);
				
			    viewInputList.add(viewInput);
			}
			
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return viewInputList;
	}
	
	//This method generates a text file contains file wise clone and line count
	public void GenerateViewInput(String NiCadFileForType1Clone,String NiCadFileForType2Clone,String NiCadFileForType3Clone,String NiCadFileForFileWiseLineCount,boolean isDom)
	{		
		List<ViewInput> viewInputList = new ArrayList<ViewInput>();
		List<CodeFragment> codeFragmentListType1Clone=new ArrayList<CodeFragment>();
		List<CodeFragment> codeFragmentListType2Clone=new ArrayList<CodeFragment>();
		List<CodeFragment> codeFragmentListType3Clone=new ArrayList<CodeFragment>();
		
		LineCounterOfFolders lineCounterOfFolders=new LineCounterOfFolders();
		
		/** Counting file wise line for**/
		List<CodeFragment> codeFragmentListAllClones=new ArrayList<CodeFragment>();
		/** reading arguments for input files**/
		viewInputList=new LineCounterOfFolders().GetFileWiseLineCount(NiCadFileForFileWiseLineCount, isDom);
		
		/** Counting clone for Type1,Type2 and Type3 clones**/
		codeFragmentListType1Clone=lineCounterOfFolders.xmlPraseForNicadResult(NiCadFileForType1Clone);
		codeFragmentListType2Clone=lineCounterOfFolders.xmlPraseForNicadResult(NiCadFileForType2Clone);
		codeFragmentListType3Clone=lineCounterOfFolders.xmlPraseForNicadResult(NiCadFileForType3Clone);
		
		codeFragmentListAllClones.addAll(codeFragmentListType1Clone);
		codeFragmentListAllClones.addAll(codeFragmentListType2Clone);
		codeFragmentListAllClones.addAll(codeFragmentListType3Clone);
		
		/** Merging Line count and clone count in viewinput object **/
		for(ViewInput viewInput:viewInputList )
		{
			int fileWiseCloneCount=0;
			for(CodeFragment codeFragment:codeFragmentListAllClones)
			{
				if(viewInput.fullDirectory.trim().contentEquals(codeFragment.getSrcFile().trim()))
				{
					fileWiseCloneCount++;
				}
			}

			viewInputList.get(viewInputList.indexOf(viewInput)).cloneCount=fileWiseCloneCount;
			
			}
		
		String FileWiseLineAndCloneCount="";
		
		for(ViewInput viewInput:viewInputList )
		{
			FileWiseLineAndCloneCount=FileWiseLineAndCloneCount+viewInput.fileName+" "+viewInput.fullDirectory+" "+viewInput.lineCount+" "+viewInput.cloneCount+"\n"; 
	//		System.out.println(viewInput.fileName+"  clone:"+viewInput.cloneCount+ " line:"+viewInput.lineCount+" dir"+viewInput.fullDirectory);
		}
		
		/** Writing viewinput object to file**/
		try 
		{
			String filename = "ViewInput.txt";
			FileWriter fileWriter = new FileWriter(filename, false);
																
			fileWriter.write(FileWiseLineAndCloneCount);
																			
			fileWriter.close();
		} catch (IOException ioe) 
		{
			System.err.println("IOException: " + ioe.getMessage());
		}	
	}
}
