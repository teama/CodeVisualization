package org.agile6990.ParseXML;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

//import com.sun.org.apache.xalan.internal.xsltc.runtime.Hashtable;

import org.agile6990.model.CodeFragment;
import org.agile6990.model.ComparatorByIncreasingSize;
import org.agile6990.model.ViewInput;

/**
 * This program takes as input NiCad's generated XML files for code block and clone information.
 * @output: return count of line and clone for each file in a system  
 * @author Rakib
 * 
 */

public class LineCounterOfFolders {

public static Map<String,Integer> fileWiseLineCount;
	
//Calculate and return file wise line count
	public List<ViewInput> GetFileWiseLineCount(String dir, boolean isDom) {
			String file = dir;

			List<ViewInput> viewInputList = new ArrayList<ViewInput>();
			
			List<CodeFragment> fragmentsList = null;
			
			//start parsing of XML using STAX
			fragmentsList = parseWithSTAX(file);

			//Getting file wise line count from 
			fileWiseLineCount=GetFileWiseLineCount(fragmentsList);
			
			for (Map.Entry<String, Integer> entry : fileWiseLineCount.entrySet())
			{
				ViewInput viewInput=new ViewInput();
				
				viewInput.fullDirectory=entry.getKey();
				viewInput.lineCount=entry.getValue();
				viewInput.fileName=entry.getKey().substring(entry.getKey().lastIndexOf("/")+1);
	
			    viewInputList.add(viewInput);
			}
			
			return viewInputList;
	}
	
	//Parse XML file using  STAX
	public List<CodeFragment> parseWithSTAX(String fileName) {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader streamReader;
		List<CodeFragment> fragmentsList = null;
		try {

			FileInputStream fileInputStream = new FileInputStream(fileName);
			streamReader = inputFactory.createXMLStreamReader(fileInputStream);
			fragmentsList = processStream(streamReader);
			streamReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		return fragmentsList;
	}

	/**
	 * Returns an IMMUTABLE list of CodeFragments SORTED in ascending order of
	 * sizes
	 * 
	 * @param dom
	 * @return
	 */
	private List<CodeFragment> processStream(XMLStreamReader streamReader) {
		// int fragmentCounter = -1;
		CodeFragment codeFragment = null;
		List<CodeFragment> fragmentsList = new ArrayList<CodeFragment>();

		try {
			while (streamReader.hasNext()) {
				int eventCode = streamReader.next();
				switch (eventCode) {
				case 1:
					// System.out.println("\n event: START_ELEMENT");
					codeFragment = processStaxStartElement(streamReader);
					// set an id to the code fragment
					// if (codeFragment != null) {
					// fragmentCounter++;
					// codeFragment.setId(fragmentCounter);
					// }
					break;
				case 2:
					// System.out.println("\n event: END_ELEMENT");
					boolean isSource = processStaxEndElement(streamReader);
					if (isSource && codeFragment != null && codeFragment.isCompletelyPopulated()) {
						fragmentsList.add(codeFragment);
					}
					break;
				case 4:
					// System.out.println("\n event: CHARACTER_ELEMENT");
					String data = processStaxCharacterElement(streamReader);
					// System.out.println(data);
					if (data.length() > 0 && codeFragment != null) {
						codeFragment.setContent(data);
					}
					break;
				}

			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

		Collections.sort(fragmentsList, ComparatorByIncreasingSize.getInstance());
		return Collections.<CodeFragment> unmodifiableList(fragmentsList);
	}

	private CodeFragment processStaxStartElement(XMLStreamReader streamReader) {
		CodeFragment codeFragment = null;
		String localName = streamReader.getLocalName();
		// System.out.println("Localname: " + localName);
		if (localName.equalsIgnoreCase("source")) {
			int attributeCount = streamReader.getAttributeCount();
			String srcFile = null;
			int startLine = -1;
			int endLine = -1;
			int id = -1;
			String attributes = "";
			for (int i = 0; i < attributeCount; i++) {
				String attrName = streamReader.getAttributeLocalName(i);
				String attrValue = streamReader.getAttributeValue(i);
				attributes = attributes + "\n" + attrName + "=" + attrValue;

				if (attrName.equalsIgnoreCase("file")) {
					srcFile = attrValue;
				} else if (attrName.equalsIgnoreCase("startline")) {
					startLine = Integer.parseInt(attrValue);
				} else if (attrName.equalsIgnoreCase("endline")) {
					endLine = Integer.parseInt(attrValue);
				} else if (attrName.equalsIgnoreCase("id")) {
					id = Integer.parseInt(attrValue);
				}
			}
			// System.out.println("Attributes: " + attributes);
			codeFragment = new CodeFragment(id, srcFile, startLine, endLine);
		}
		return codeFragment;
	}

	private boolean processStaxEndElement(XMLStreamReader streamReader) {
		String localName = streamReader.getLocalName();
		// System.out.println("Localname: " + localName);
		if (localName.equalsIgnoreCase("source")) {
			return true;
		}
		return false;
	}

	private String processStaxCharacterElement(XMLStreamReader streamReader) {
		String data = streamReader.getText().trim();
		return filterBlankLines(data);
		// return data;
	}

	private String filterBlankLines(String data) {
		StringBuilder sb = new StringBuilder();
		String[] lines = data.split("\n");
		for (int i = 0; i < lines.length; i++) {
			if (lines[i].trim().length() == 0) {
				// skip blank line
				continue;
			}
			sb.append(lines[i]);
			if (i < lines.length - 1) {
				sb.append("\n");
			}
		}

		return sb.toString();
	}
	
	private Map<String,Integer> GetFileWiseLineCount(List<CodeFragment> CodeFragments)
	{		
		Map<String,Integer> fileWiseLineCount = new HashMap<String, Integer>();
		
		for (CodeFragment codeFragment : CodeFragments) {
			
		/*	if(codeFragment.getSrcFile().contains("cobertura/util/StringUtil.java"))
			{
				
				System.out.println("ENDLINE"+fileWiseLineCount.get(codeFragment.getSrcFile()));
			}*/
			
			if(fileWiseLineCount.containsKey(codeFragment.getSrcFile()))
			{
					if (fileWiseLineCount.get(codeFragment.getSrcFile())<codeFragment.getEndLine())
						{
							fileWiseLineCount.put(codeFragment.getSrcFile(), codeFragment.getEndLine());
						}
			}
			else
			{
				fileWiseLineCount.put(codeFragment.getSrcFile(), codeFragment.getEndLine());
			}
		}
		
		return fileWiseLineCount;
	}
	
	//Parsing XML of NiCAD Clone output
	public List<CodeFragment> xmlPraseForNicadResult(String fileName)
	{	
		Set<CodeFragment> fragmentsFromNicadResult = new HashSet<CodeFragment>();

		Document document = null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			try
			{
				DocumentBuilder builder = factory.newDocumentBuilder();
				document = builder.parse(new File(fileName));
			}
			catch (ParserConfigurationException pce)
			{
				pce.printStackTrace();
			} 
			catch (SAXException se) 
			{
				se.printStackTrace();
			} 
			catch (IOException ioe) 
			{
				ioe.printStackTrace();
			}
			
			document.getDocumentElement().normalize();

			NodeList cloneList = document.getElementsByTagName("clone");

			for(int i = 0; i < cloneList.getLength(); i++)
			{
				Node clone = cloneList.item(i);

				if(clone.getNodeType() == Node.ELEMENT_NODE)
				{
					Element cloneElement = (Element) clone;
					NodeList sourceList = cloneElement.getElementsByTagName("source");

					for(int j = 0; j < sourceList.getLength(); j++)
					{
						Node source = sourceList.item(j);

						if(source.getNodeType() == Node.ELEMENT_NODE)
						{
							Element sourceElement = (Element) source;

							String fileNameOfThisFile = sourceElement.getAttribute("file");
							int startLine = Integer.parseInt(sourceElement.getAttribute("startline"));
							int endLine =  Integer.parseInt(sourceElement.getAttribute("endline"));

							CodeFragment codeFragmentsFromNicad = new CodeFragment(fileNameOfThisFile, startLine, endLine);
							fragmentsFromNicadResult.add(codeFragmentsFromNicad);
						}
					}
				}
			}
		return new ArrayList<CodeFragment>(fragmentsFromNicadResult);
	}

}