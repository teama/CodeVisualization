/**
 * @author Naveen Kumar Gorla
 * @version 1.0 (Initial Version)
 * @category Junit Test Cases
 * @since 5-Nov-2015
 */

package AgileCodeVisualizationJunits;

import static org.junit.Assert.*;

import org.junit.Test;

import AgileCodeVisualization.*;

public class sTokenizerTest {

	@Test
	public void TparserTest() {
		sTokenizer sToken = new sTokenizer();
		String tempStr1 = "../QualitasCorpus-20130901r";
		String tempStr2 = sToken.Tparser(tempStr1);
		assertEquals("../QualitasCorpus-20130901r", tempStr2);
	}
}
