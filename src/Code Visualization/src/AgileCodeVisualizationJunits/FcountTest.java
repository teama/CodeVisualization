/**
 * @author Naveen Kumar Gorla
 * @version 1.0 (Initial Version)
 * @category Junit Test Cases
 * @since 5-Nov-2015
 */
package AgileCodeVisualizationJunits;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;

import AgileCodeVisualization.*;

public class FcountTest {
	
	Fcount fileCount = new Fcount();
	//int flag;

	@Test
	public void FcountContains() {
		ArrayList<String> FolderList = new ArrayList<String>();
		FolderList.add("sup1");
		FolderList.add("sup2");
		FolderList.add("sup3");
		
		//System.out.println((Fcount.folderCount ("sup1", FolderList)).size());
		//Returns true if the list contains the given string
		assertEquals((Fcount.folderCount ("sup1", FolderList)).size(), 3);	
		
	}
	
	@Test
	public void FcountNotContains() {
		ArrayList<String> FolderList = new ArrayList<String>();
		FolderList.add("sup1");
		FolderList.add("sup2");
		FolderList.add("sup3");
		
		//System.out.println((Fcount.folderCount ("sup1", FolderList)).size());
		//Returns true if the list doesn't contain the given string
		assertEquals((Fcount.folderCount ("sup4", FolderList)).size(), 4);	
		
	}

}
