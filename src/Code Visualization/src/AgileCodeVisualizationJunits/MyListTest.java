/**
 * @author Naveen Kumar Gorla
 * @version 1.0 (Initial Version)
 * @category Junit Test Cases
 * @since 10-Nov-2015
 */

package AgileCodeVisualizationJunits;

import static org.junit.Assert.*;

import org.junit.Test;

import AgileCodeVisualization.*;

public class MyListTest {
	
	Mylist MyList = new Mylist();

	@Test
	public void MyList() {
		int count = 0;
		String str = "nav/een/";
		count = MyList.backslash(str);
		assertEquals(2, count);		
	}

}
