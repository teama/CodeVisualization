package AgileCodeVisualizationJunits;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import AgileCodeVisualization.*;


public class fileWriteTest {

	@Test
	public void CreateFileTest() {
		File file = new File("StrtEnd.txt");
		assertTrue(file.exists());
	}
	
	@Test
	public void UniqueFileTest() {
		File file = new File("Uniq.txt");
		assertTrue(file.exists());
	}
	
	@Test
	public void TokenTest() {
		File file = new File("tokens.txt");
		assertTrue(file.canWrite());
	}

}
