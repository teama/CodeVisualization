package AgileCodeVisualization;
import java.io.*;
import java.util.ArrayList;
/**
Writes percentages clone count and next directory to output file

author: deepak reddy ekkati
**/
public class diffold {
	public void Ffinal(int d)throws  Exception{
		try{
			ArrayList<String> list = new ArrayList<String>();
			BufferedReader in = new BufferedReader(new FileReader("Fcount.txt")); //new fie reader
		    String str;
		    int temp31 = 0;
			while((str = in.readLine()) != null)
	        {
	        list.add(str);// saving values in an array list
		
	    	}
			 ArrayList<String> listnum = new ArrayList<String>();
			    ArrayList<Integer> listnum1 = new ArrayList<Integer>();
			    
			    ArrayList<String> liststr = new ArrayList<String>();
			    
			    
			    for(int i=0;i<list.size();i++)
				{
			    	int t = 0;
				int x =list.get(i).length();
				for(int temp22=0;temp22<x;temp22++){
					if(list.get(i).charAt(temp22)==32){
						 t=temp22;	
						 String st1=list.get(i).substring(0,(t));
							listnum.add(st1);
							String st2=list.get(i).substring((t+1),x);
							liststr.add(st2);
					}
					
				}
			 for(int p=0;p<listnum.size();p++)
			    {
			    	int j=Integer.parseInt(listnum.get(p));
			    	
			    	if(j>1)
			    	{
			    		j--;
			    	}
			    	//System.out.println(j);
			    	listnum1.add(p, j);
			    	
			    }
			 //System.out.println(listnum1);
			 
			 for(int temp15=0;temp15<liststr.size();temp15++){
				 ArrayList<String> listnum2 = new ArrayList<String>();
				 ArrayList<String> inputp = new ArrayList<String>();
				 String strtemp15=liststr.get(temp15);
				 ArrayList<Integer> index2 = new ArrayList<Integer>();
				 int bc1=0;
				 String st29 = null;
				 String filename=temp15+".txt";
				 BufferedWriter bWriter=new BufferedWriter(new FileWriter(filename));
				 bWriter.write(strtemp15);
				 bWriter.newLine();
				 bc1=new Mylist().backslash(strtemp15);
				 for (int temp16=temp15;temp16<liststr.size();temp16++){
					String strtemp16=liststr.get(temp16);
					
					int bc2=0;
					bc2=new Mylist().backslash(strtemp16);
					if((bc2==bc1+1)&&(strtemp16.contains(strtemp15))){ //Checking if back slashes are grater and is a substring
						inputp.add(strtemp16);
						index2.add(liststr.indexOf(strtemp16));
						listnum2.add(listnum.get(temp16));
						st29=strtemp16;
						
					}
				 }
				 int sum1=0;
				 for(int temp17=0;temp17<listnum2.size();temp17++){
					 int temp18=Integer.parseInt(listnum2.get(temp17));
					 sum1=sum1+temp18;
				 }

				 float [] f1=new float[10];
				 for(int temp19=0;temp19<listnum2.size();temp19++){
					f1[temp19]=((Float.parseFloat(listnum2.get(temp19)))/sum1)*100;
					int index3=index2.get(temp19);
					String strI = Float.toString(f1[temp19]);
					
					//function call for clone count
					temp31=new FolderWiseCount().ccount(inputp.get(temp19)); 
					bWriter.write(strI+" "+temp31+" "+index3); //Buffer writer to print percentage and filename
					bWriter.newLine();
				 }
				 bWriter.close();
			 }
		}
		}
		
		catch (Exception e) {
			e.printStackTrace();
		    }
		}
}
