package AgileCodeVisualization;
import java.io.*;
import java.util.*;
public class GetFolderList {

	public ArrayList<String> GetFolderList(String folderFilePath)
	{
	    ArrayList<String> folderList = new ArrayList<String>();
	   
	try {
	File file = new File(folderFilePath);
	FileReader fileReader = new FileReader(file);
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	String line;
	int i=0;

	while ((line = bufferedReader.readLine()) != null) 
	{
	folderList.add(line.trim());
	i=i+1;
	}

	fileReader.close();
	} catch (IOException e) 
	{
	e.printStackTrace();
	}

	return folderList;
	}
}
