package AgileCodeVisualization;
import java.util.*;
import java.io.*;
/**
Calls all other functions and prints unique paths
Author: Kalyan
**/

public class fileWrite {
	public void CalcUniq(int fsile) 
	{
		try {
			BufferedReader reader =new BufferedReader( new FileReader("StrtEnd.txt"));
	        Set <String> lines = new HashSet<String>(5000);
	        String line;
	        while((line=reader.readLine())!= null){
	        	lines.add(line);
	        }
	        reader.close();
		BufferedWriter writer = new BufferedWriter(new FileWriter("Uniq.txt"));
		for(String unique : lines){
			writer.write(unique);
			writer.newLine();
		}
		writer.close();
		new fileCounter().Tokenizer(1);
		new nFiles().Foldercount(1);
		new PercentageCounter().FolderC(1);
		new substring().cCount(1);
		new diffold().Ffinal(1);
		}
		catch (Exception e) {
			e.printStackTrace();
		    }
}
}