package AgileCodeVisualization;
import java.io.*;
import java.util.*;
public class GetViewInput {
	public List<ViewInput> GetViewInput(String viewFilePath)
	{
	List<ViewInput> viewInputList = new ArrayList<ViewInput>();

	try {
	File file = new File(viewFilePath);
	FileReader fileReader = new FileReader(file);
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	String line;

	while ((line = bufferedReader.readLine()) != null) {
	String [] elementOfViewInput=line.split("\\s+");

	ViewInput viewInput=new ViewInput();
	viewInput.fileName=elementOfViewInput[0];
	viewInput.fullDirectory=elementOfViewInput[1];
	viewInput.lineCount=Integer.valueOf(elementOfViewInput[2]);
	viewInput.cloneCount=Integer.valueOf(elementOfViewInput[3]);

	    viewInputList.add(viewInput);
	}

	fileReader.close();
	} catch (IOException e) {
	e.printStackTrace();
	}
	return viewInputList;
	}

}
