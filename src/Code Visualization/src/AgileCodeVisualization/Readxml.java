package AgileCodeVisualization;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
public class Readxml {

/**

Main class 
taking the clone.xml file and creating StrtEnd.txt with all the clone folder paths 
**/
	 public static void main(String argv[]) 
		{
			try {
				File fXmlFile = new File("clone.xml");
				File file=new File("StrtEnd.txt");
				if(!file.exists()){
					file.createNewFile();
				}
				FileWriter fw=new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw= new BufferedWriter(fw);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();
				
				NodeList nList = doc.getElementsByTagName("source");
				
				
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						
						bw.write(eElement.getAttribute("file"));
						bw.newLine();
						}
					}
				bw.close();

				//calling fileWrite.java
				new fileWrite().CalcUniq(1);
	  			} 
				catch (Exception e) {
		e.printStackTrace();
	    }
		}
}