package AgileCodeVisualization;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.*;
/**
calculating the clone count in every folder

**/

public class substring {
	
	public void cCount(int x)
	{
	
	String  folderWiseCloneCount="";

	
	substring s=new substring();
	
	List<ViewInput> viewInputList=s.GetViewInput("ViewInput.txt");
	        ArrayList FolderList=s.GetFolderList("tokens.txt");


	List<ViewInput> FolderWiseCloneCount=new ArrayList<ViewInput>();

	for(int i=0;i<FolderList.size();i++)
	{
	

	ViewInput curentfolder=new ViewInput();

	curentfolder.fullDirectory=FolderList.get(i).toString();

	for(ViewInput viewInput:viewInputList )
	{
	if(viewInput.fullDirectory.contains(FolderList.get(i).toString()))
	{

	curentfolder.cloneCount+=viewInput.cloneCount;
	}
	}

	FolderWiseCloneCount.add(curentfolder);

	}



	for(ViewInput viewInput:FolderWiseCloneCount )
	{
	

	folderWiseCloneCount=folderWiseCloneCount+viewInput.fullDirectory+" "+ viewInput.cloneCount+"\r\n"; 

	}
	System.out.println(folderWiseCloneCount); 

	/** Writing viewinput object to file**/
	try 
	{
	String fileName="FolderWiseCloneCount";
	FileWriter fileWriter = new FileWriter(fileName, false);

	fileWriter.write(folderWiseCloneCount);
	
	fileWriter.close();
	} catch (IOException ioe) 
	{
	System.err.println("IOException: " + ioe.getMessage());
	}


	}

	public ArrayList GetFolderList(String folderFilePath) {
		 ArrayList<String> folderList = new ArrayList<String>();
		   
		 try {
		 File file = new File(folderFilePath);
		 FileReader fileReader = new FileReader(file);
		 BufferedReader bufferedReader = new BufferedReader(fileReader);

		 String line;
		 int i=0;

		 while ((line = bufferedReader.readLine()) != null) 
		 {
		 folderList.add(line.trim());
		 i=i+1;
		 }

		 fileReader.close();
		 } catch (IOException e) 
		 {
		 e.printStackTrace();
		 }

		 return folderList;
	}

	public List<ViewInput> GetViewInput(String FilePath) {
		List<ViewInput> viewInputList = new ArrayList<ViewInput>();

		try {
		File file = new File(FilePath);
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		String line;

		while ((line = bufferedReader.readLine()) != null) {
		String [] elementOfViewInput=line.split("\\s+");

		ViewInput viewInput=new ViewInput();
		viewInput.fileName=elementOfViewInput[0];
		viewInput.fullDirectory=elementOfViewInput[1];
		viewInput.lineCount=Integer.valueOf(elementOfViewInput[2]);
		viewInput.cloneCount=Integer.valueOf(elementOfViewInput[3]);

		    viewInputList.add(viewInput);
		}

		fileReader.close();
		} catch (IOException e) {
		e.printStackTrace();
		}
		return viewInputList;
	}

	}
